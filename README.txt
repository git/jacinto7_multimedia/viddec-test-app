=======
PREREQ:
=======
* cross compiler is in PATH, eg:
	export PATH=$PATH:/opt/gcc-arm-8.3-2019.03-x86_64-aarch64-linux-gnu/bin
* Modify the Makefile on to point to the proper directory on your machine or pass from command line:
	DRMHEADERS --> should point to location of the DRM headers, eg:
		DRMHEADERS = $HOME/buildroot/output/host/usr/aarch64-buildroot-linux-gnu/sysroot/usr
* Modify the Makefile on to point to location of the Kernel headers (linux source code path), eg:
        KERNELHEADERS = $HOME/ti-linux-kernel
======
BUILD:
======
make

=======
OUTPUT:
=======
tidec_decode binary

=============
SETUP TARGET:
=============
* Copy the output binary to the target rootfs
* The following modules must also be present on the target:
	- videobuf2-common.ko
	- videobuf2-memops.ko
	- videobuf2-v4l2.ko
	- videobuf2-dma-sg.ko
	- videobuf2-dma-contig.ko
	- v4l2-mem2mem.ko
	- vxd-dec.ko

========
EXECUTE:
========
* On the target:
	# insmod videobuf2-common.ko
	# insmod videobuf2-memops.ko
	# insmod videobuf2-v4l2.ko
	# insmod videobuf2-dma-sg.ko
	# insmod videobuf2-dma-contig.ko
	# insmod v4l2-mem2mem.ko
	# insmod vxd-dec.ko
	# ./tidec_decode
* On successful completion, you will see this trace:
	"test app completed successfully"

==============
RETURN VALUES:
==============
- 0: test app completed successfully
- EXIT_FAILURE: test app failed to complete successfully


* Reference: credentiality2.blogspot.com/2010/04/v4l2-example.html

